package Visitante;

import EntradaMercadoria.Fator;
import EntradaMercadoria.Markup;
import EntradaMercadoria.Produto;

public class Item implements ItemVisitante {
    private double valor;
    private double quantidade;
    private double quantidadeVenda;
    private String codigoProduto;
    private String EAN;

    public Item(double valor, double quantidade, String EAN) {
        this.valor = valor;
        this.quantidade = quantidade;
        this.EAN = EAN;
    }

    @Override
    public void formaPreco(Markup markup) {
        valor =  (valor + markup.getDespesa()) * markup.getMargemLucro() / 100;
    }

    @Override
    public void fatorConversao(Fator fator) {
        if (fator.getOperador().equals("divisão")) {
            quantidadeVenda = quantidade / fator.getValor();
        } else if(fator.getOperador().equals("multiplicação")) {
            quantidadeVenda = quantidade * fator.getValor();
        }
    }

    @Override
    public void correlacao(Produto produto) {
        codigoProduto = "";
        if (EAN.equals(produto.getCodigoBarras())) {
            codigoProduto = produto.getCodigo();
        }
    }

    @Override
    public String gerar() {
        return toString();
    }

    @Override
    public String toString() {
        return "Item{" +
                "valor=" + valor +
                ", quantidade=" + quantidade +
                ", quantidadeVenda=" + quantidadeVenda +
                ", codigoProduto='" + codigoProduto + '\'' +
                ", EAN='" + EAN + '\'' +
                '}';
    }
}
