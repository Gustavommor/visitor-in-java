package Visitante;

import EntradaMercadoria.Fator;
import EntradaMercadoria.Markup;
import EntradaMercadoria.Produto;

public interface ItemVisitante {
    void formaPreco(Markup markup);
    void fatorConversao(Fator fator);
    void correlacao(Produto produto);
    String gerar();
}
