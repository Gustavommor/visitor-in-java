package Elemento;

import EntradaMercadoria.Fator;
import EntradaMercadoria.Markup;
import EntradaMercadoria.Produto;
import Visitante.ItemVisitante;

public class GerarPreEntrada implements ElementoVisitado {
    @Override
    public Object GerarEntrada(ItemVisitante item) {
        item.correlacao(new Produto("000001","1234567891234","Produto teste"));
        item.fatorConversao(new Fator("divisão",10));
        item.formaPreco(new Markup(10, 100));
        return item.gerar();
    }
}
