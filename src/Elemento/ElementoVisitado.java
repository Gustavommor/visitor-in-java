package Elemento;

import Visitante.ItemVisitante;

public interface ElementoVisitado {
    public Object GerarEntrada(ItemVisitante item);
}
