package EntradaMercadoria;

public class Fator {
    private String operador;
    private double valor;

    public Fator(String operador, double valor) {
        this.operador = operador;
        this.valor = valor;
    }

    public String getOperador() {
        return operador;
    }

    public double getValor() {
        return valor;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
