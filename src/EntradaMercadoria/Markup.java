package EntradaMercadoria;

public class Markup {
    private double margemLucro;
    private double despesa;

    public Markup(double margemLucro, double despesa) {
        this.margemLucro = margemLucro;
        this.despesa = despesa;
    }

    public double getMargemLucro() {
        return margemLucro;
    }

    public double getDespesa() {
        return despesa;
    }

    public void setMargemLucro(double margemLucro) {
        this.margemLucro = margemLucro;
    }

    public void setDespesa(double despesa) {
        this.despesa = despesa;
    }
}
