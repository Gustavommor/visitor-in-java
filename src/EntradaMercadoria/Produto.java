package EntradaMercadoria;

public class Produto {
    private String codigo;
    private String codigoBarras;
    private String nome;

    public Produto(String codigo, String codigoBarras, String nome) {
        this.codigo = codigo;
        this.codigoBarras = codigoBarras;
        this.nome = nome;
    }

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public String getCodigo() {
        return codigo;
    }

    @Override
    public String toString() {
        return nome;
    }
}
