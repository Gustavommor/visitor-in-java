import Elemento.ElementoVisitado;
import Elemento.GerarPreEntrada;
import Visitante.Item;
import Visitante.ItemVisitante;

public class Main {
    public static void main(String[] args) {
        ElementoVisitado elementoVisitado = new GerarPreEntrada();
        ItemVisitante item = new Item(12.50, 5,"1234567891234");
        System.out.println(elementoVisitado.GerarEntrada(item));
    }
}
